const { resolve } = require('path');
<%_ if (css) { -%>
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
<%_ } -%>
<% if (svg) { -%>
const SvgStorePlugin = require('external-svg-sprite-loader');
<%_ } -%>

module.exports = {
	entry: {
		appJs: './src/js/app.js'
	},

	module: {
		rules: [
			<%_ if (css) { -%>
			{
				test: /\.css$/,

				use: [
					{
						loader: MiniCssExtractPlugin.loader
					},
					'css-loader'
				]
			},
			<%_ } -%>
			<%_ if (font) { -%>
			{
				test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,

				use: [
					{
						loader: 'file-loader',

						options: {
							name: '[name].[ext]',
							outputPath: './fonts',
							publicPath: '../fonts'
						}
					}
				]
			},
			<%_ } -%>
			<%_ if (svg) { -%>
			{
				test: /\.svg$/,

				use: [
					{
						loader: SvgStorePlugin.loader,

						options: {
							name: 'svg/sprite.svg',
							publicPath: '../'
						}
					}
				]
			},
			<%_ } -%>
		]
	},

	output: {
		filename: 'js/app.js',
		path: resolve(__dirname, 'dist')
	},

	plugins: [
		<%_ if (css) { -%>
		new MiniCssExtractPlugin({
			filename: 'css/app.css'
		}),
		<%_ } -%>
		<%_ if (svg) { -%>
		new SvgStorePlugin(),
		<%_ } -%>
	]
}
