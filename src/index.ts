import Generator from 'yeoman-generator'

module.exports = class SimpleWebpackGenerator extends Generator {
  answers: any
  templates: string[]

  constructor (args, opts) {
    super(args, opts)
    this.env.options.nodePackageManager = 'yarn'
    this.templates = [
      '.gitignore',
      'src/js/'
    ]
  }

  async prompting (): Promise<void> {
    this.answers = await this.prompt([
      {
        type: 'input',
        name: 'name',
        message: 'Application name'
      },
      {
        type: 'confirm',
        name: 'css',
        message: 'CSS support'
      },
      {
        type: 'confirm',
        name: 'font',
        message: 'Font support',
        when: answers => {
          return answers.css
        }
      },
      {
        type: 'confirm',
        name: 'svg',
        message: 'SVG support',
        when: answers => {
          return answers.css
        }
      }
    ])

    if (this.answers.css as boolean) {
      this.templates.push('src/css/')
    }
  }

  async writing (): Promise<void> {
    await this.addDevDependencies({
      webpack: '5.30.0',
      'webpack-cli': '4.6.0'
    })

    if (this.answers.css as boolean) {
      await this.addDevDependencies({
        'css-loader': '5.2.0',
        'mini-css-extract-plugin': '1.4.0'
      })
    }
    if (this.answers.font as boolean) {
      await this.addDevDependencies({ 'file-loader': '6.2.0' })
    }
    if (this.answers.svg as boolean) {
      await this.addDevDependencies({ 'external-svg-sprite-loader': '7.1.2' })
    }

    this.packageJson.merge({
      name: this.answers.name,
      scripts: {
        development: './node_modules/.bin/webpack --mode=development',
        production: './node_modules/.bin/webpack --mode=production'
      }
    })

    this.fs.copyTpl(this.templatePath('webpack.config.js.t'), this.destinationPath('webpack.config.js'), this.answers)

    this.templates.forEach(template => {
      this.fs.copy(
        this.templatePath(template),
        this.destinationPath(template)
      )
    })
  }
}
